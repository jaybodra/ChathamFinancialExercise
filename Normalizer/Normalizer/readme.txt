﻿TODO
-Implement the PercentNormalizer
-Verify proper operation of the PercentNormalizer in accordance with the provided specification (in the shell class)
-Verify ILogger.Log(Exception) is called when the normalization is not successful
-Verify ILogger.Log(string) is called when the normalization is successful
