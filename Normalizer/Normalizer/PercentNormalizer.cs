using System;
using System.Text.RegularExpressions;

namespace Normalizer
{
    public interface INormalizer
    {
        string Normalize(string value);
    }

    public class PercentNormalizer : INormalizer
    {
        /// <summary>
        ///     Normalizes a string value for a percentage expressed as a number with or without a percent symbol
        ///     to a decimal string representation.
        ///     -Whitespace is ignored.
        ///     -If present, the percent sign must appear after the number.
        ///     -Commas may be used a number separator
        /// </summary>
        /// <param name="value">The value To normlaize</param>
        /// <returns>Normalized percent as a decimal string</returns>
        /// <exception cref="ArgumentNullException">The value is null, empty or whitespace.</exception>
        /// <exception cref="FormatException">The value is not a valid percent representation.</exception>
        public string Normalize(string value)
        {
            //Examples:
            //  given "12%" returns "0.12"
            //  given "1,345,678.001234" returns "1345678.001234"
            //  given "% 12" throws FormatException
            //  given "  " throws ArgumentNullException
            //  given "        0.1  \t        % " returns "0.001"
            //  given "-1" returns "-1"

            //All the above cases are implement and validated using unit testing
            //Algorithm:
            //1. remove the whitespace, tabs using regular expresion.
            //2. Check for valid real number using regular exception.
            //3. Convert the string to a number representation.
            //4. for a number with valid percent sign return the percentage number in string format.
            //5. number without percent sign return the number in string format.
            if (value.Equals(" ") || String.IsNullOrEmpty(value))
                throw new ArgumentNullException("The value is null, empty or whitespace.");

            string pattern = "[\\s]+";
            string replacement = "";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(value, replacement);
           
            rgx = new Regex(@"^(-)?[0-9,]*(\.\d+)?(%)?$");
            double parsedValue;

            if (!rgx.IsMatch(result))
                throw new FormatException("The value is not a valid percent representation.");
            else if (result.EndsWith("%")) {
                parsedValue = ToNumber(result, true);
                result = (parsedValue / 100d).ToString();
            }
            else
            {
                parsedValue = ToNumber(result, false);
                result = (parsedValue).ToString();
            }

            return result;
        }

        public double ToNumber(string numString, bool percentPresent)
        {
            double num;
            if (percentPresent)
                numString = numString.Substring(0, numString.Length - 1);
            else
                numString = numString.Substring(0, numString.Length);

            bool status = double.TryParse(numString, out num);
            if (!status)
                throw new FormatException("Unable to parse string to number.");
            return num;
        }
    }
}